from django.db import models
from django.utils import timezone

# Create your models here.
class Group(models.Model):

    name = models.CharField(max_length=20)
    description = models.CharField(max_length=200)

    def __str__(self):
        return self.name

class User(models.Model):

    username = models.CharField(max_length=50)
    created = models.DateField(default=timezone.now().strftime("%Y-%m-%d"))
    group = models.ForeignKey(Group, on_delete=models.CASCADE)

    def __str__(self):
        return self.username

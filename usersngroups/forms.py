from django import forms
from .models import Group, User

class GroupForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = ['name', 'description']

class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['username', 'group']

from django.urls import path
from . import views

urlpatterns = [
    path(r'', views.UsersView.as_view(), name="users"), #default to 'users' page
    path(r'users/', views.UsersView.as_view(), name="users"),
    path(r'groups/', views.GroupsView.as_view(), name="groups"),
    path(r'update_user/', views.UpdateUserView.as_view(), name="update_user"),
    path(r'remove_user/', views.RemoveUser.as_view(), name="remove_user"),
    path(r'update_group/', views.UpdateGroupView.as_view(), name="update_group"),
    path(r'remove_group/', views.RemoveGroup.as_view(), name="remove_group"),
]

from django.views.generic import TemplateView
from django.shortcuts import render, redirect
from .models import Group, User
from .forms import GroupForm, UserForm

################################################################################

class UsersView(TemplateView):
    template_name = 'users.html'

    def get(self, request):
        form = UserForm()
        users = User.objects.all()
        groups = Group.objects.all()
        return render(request, self.template_name, {'form': form,
                                                    'users': users})

    def post(self, request):
        form = UserForm(request.POST)
        users = User.objects.all()
        if form.is_valid():
            form.save()
            form = UserForm()
            return render(request, self.template_name, {'form': form,
                                                        'users': users})

class UpdateUserView(TemplateView):
    template_name = 'update_user.html'

    def get(self, request):
        user_id = int(request.GET.get('user_id'))
        user = User.objects.get(id=user_id)
        default_name = getattr(user, 'username')
        default_group = getattr(user, 'group')
        form = UserForm(initial={'username': default_name, 'group': default_group})
        return render(request, self.template_name, {'form': form, 'user': user})

    def post(self, request):
        form = UserForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['username']
            group = form.cleaned_data['group']
            user_id = int(request.POST.get('user_id'))
            User.objects.filter(id=user_id).update(username=name, group=group)

        return redirect("/users/")

class RemoveUser(TemplateView):
    def post(self, request):
        form = UserForm()
        user_id = int(request.POST.get('user_id'))
        user = User.objects.get(id=user_id)
        user.delete()
        return redirect("/users/")

################################################################################

class GroupsView(TemplateView):
    template_name = 'groups.html'

    def get(self, request):
        form = GroupForm()
        groups = Group.objects.all()
        return render(request, self.template_name, {'form': form,
                                                    'groups': groups})

    def post(self, request):
        form = GroupForm(request.POST)
        groups = Group.objects.all()
        if form.is_valid():
            form.save()
            form = GroupForm()
            return render(request, self.template_name, {'form': form,
                                                        'groups': groups})

class UpdateGroupView(TemplateView):
    template_name = 'update_group.html'

    def get(self, request):
        group_id = int(request.GET.get('group_id'))
        group = Group.objects.get(id=group_id)
        default_name = getattr(group, 'name')
        default_desc = getattr(group, 'description')
        form = GroupForm(initial={'name': default_name, 'description': default_desc})
        return render(request, self.template_name, {'form': form, 'group': group})

    def post(self, request):
        form = GroupForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            desc = form.cleaned_data['description']
            group_id = int(request.POST.get('group_id'))
            Group.objects.filter(id=group_id).update(name=name, description=desc)

        return redirect("/groups/")

class RemoveGroup(TemplateView):
    def post(self, request):
        form = GroupForm()
        group_id = int(request.POST.get('group_id'))
        group = Group.objects.get(id=group_id)
        #check if group has users, if yes then can't delete
        groups_with_users = User.objects.filter(group__isnull=False).values_list('group', flat=True)
        parent_groups = Group.objects.filter(id__in=list(set(groups_with_users)))
        if group not in parent_groups:
            group.delete()
        return redirect("/groups/")
